import React, {Component} from 'react';
import {Button, Card, CardBody, Col, Input, Label, Nav, NavItem, NavLink, Row, Modal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import ReactTable from 'react-table'
import '../../scss/react-table.css'
import {withAuthApiLoadingNotification} from "../../components/HOC/withLoadingAndNotification";
import {cardHeader} from '../../components/Card/CollapsibleCardHeader';
import RestApi from "../../service/RestApi";
import './style.css'
import CardHeader from "reactstrap/es/CardHeader";
import produce from "immer";
import CardFooter from "reactstrap/es/CardFooter";
import FormGroup from "reactstrap/es/FormGroup";
import FormFeedback from "reactstrap/es/FormFeedback";
import ViewLADModal from './ViewLADModal';
import {formatDate} from "../../components/utiles";
import classnames from "classnames";


const CARRIER_LIST = [
  "AAM-0866",
  "ABN-0302",
  "ADX-0414",
  "ALG-0241",
  "ALN-0044",
  "ALN-0066",
  "ALN-0444",
  "ALN-0539",
  "ALU-0006",
  "AMJ-0470",
  "AMM-0937",
  "AMN-0366",
  "AMU-0224",
  "ANN-0663",
  "ANW-0053",
  "ANW-0311",
  "ASI-0322",
  "ASI-0400",
  "ASN-0279",
  "ATC-0282",
  "ATE-0050",
  "ATX-0288",
  "ATX-0387",
  "ATX-0732",
  "ATZ-0004",
  "AUC-0966",
  "AUI-5353",
  "AVD-0260",
  "AVZ-0633",
  "AWE-0143",
  "AZC-0061",
  "AZU-0239",
  "BAX-6963",
  "BIZ-0606",
  "BMJ-0499",
  "BML-0742",
  "BNF-6369",
  "BTL-0867",
  "BUC-0368",
  "BUR-0515",
  "CAL-0550",
  "CAO-0670",
  "CDD-0741",
  "CDN-0725",
  "CDX-0662",
  "CDZ-0043",
  "CGI-0885",
  "CGP-0690",
  "CGY-0828",
  "CGZ-5078",
  "CHR-0343",
  "CIN-0891",
  "CIZ-0063",
  "CNK-0881",
  "CNO-0523",
  "COE-0900",
  "COK-0421",
  "COS-0661",
  "CPD-0410",
  "CPL-0221",
  "CPL-0723",
  "CPL-0963",
  "CPW-0073",
  "CQO-0319",
  "CRV-0351",
  "CTQ-0203",
  "CUT-0268",
  "CWI-0839",
  "CWK-0035",
  "CWL-0566",
  "CWV-0909",
  "CZZ-0586",
  "DES-0147",
  "DEY-0079",
  "DIP-1954",
  "DKC-0361",
  "DLT-0233",
  "DLX-0693",
  "DPY-0082",
  "DTR-0543",
  "DVT-0123",
  "EAS-0136",
  "ECR-0325",
  "EMI-0365",
  "ENW-0202",
  "ESB-0078",
  "ESI-0194",
  "ETM-0048",
  "EXF-0393",
  "EXL-0752",
  "EXP-0865",
  "FFM-0841",
  "FTC-0456",
  "GCN-0077",
  "GFS-0295",
  "GLD-0059",
  "GNM-0997",
  "GSP-0777",
  "GSR-0189",
  "HOG-0965",
  "HPL-0722",
  "IAN-0509",
  "IAS-0225",
  "IAS-6225",
  "IDS-6437",
  "IDW-0460",
  "IEX-0508",
  "IFA-0172",
  "IIC-6406",
  "IOZ-0331",
  "IPC-0589",
  "IPV-0127",
  "IPV-0582",
  "IRK-2121",
  "IUT-0468",
  "IUT-0478",
  "IUT-0897",
  "JJJ-0012",
  "JNT-0568",
  "JRL-0021",
  "KRB-0989",
  "KST-0545",
  "KYL-0699",
  "LCZ-0562",
  "LDB-0477",
  "LDD-0408",
  "LDL-0535",
  "LDM-0536",
  "LDW-0537",
  "LGD-0533",
  "LGK-0708",
  "LGM-0382",
  "LGT-0432",
  "LGT-0665",
  "LMI-0631",
  "LNH-0516",
  "LNK-0563",
  "LOC-6106",
  "LOM-0369",
  "LSI-0036",
  "LSP-0293",
  "LST-0205",
  "LTD-0525",
  "LTL-0253",
  "LTS-0213",
  "MAD-0086",
  "MAL-0801",
  "MCG-0888",
  "MCI-0022",
  "MCI-0222",
  "MCI-0986",
  "MCJ-0088",
  "MCK-0898",
  "MEN-0264",
  "MFZ-0440",
  "MIT-0338",
  "MIT-0996",
  "MJD-0571",
  "MMN-0932",
  "MTV-6388",
  "MWT-0902",
  "MXT-0780",
  "MZL-0818",
  "NAC-0933",
  "NBR-0448",
  "NCQ-0908",
  "NCS-0684",
  "NFL-0657",
  "NFN-0423",
  "NHC-0890",
  "NLE-0746",
  "NNC-0652",
  "NNL-0401",
  "NOE-0135",
  "NPU-0764",
  "NRD-0332",
  "NTG-0806",
  "NTH-0098",
  "NTS-0687",
  "NTT-0231",
  "NTX-0070",
  "NWD-0713",
  "NWN-0831",
  "NWT-0638",
  "NYC-0698",
  "NZT-0697",
  "OAN-0625",
  "OCO-0590",
  "ONE-0226",
  "ONR-0658",
  "OTC-0110",
  "OTX-0110",
  "OWS-0913",
  "PAC-0757",
  "PAC-0758",
  "PAY-0917",
  "PCR-0775",
  "PEN-0412",
  "PHT-0838",
  "PHX-0451",
  "PIO-0728",
  "PJC-0272",
  "PLG-0930",
  "PLS-0767",
  "PMG-0249",
  "PRO-0390",
  "PSA-0045",
  "PUR-5536",
  "PUR-5674",
  "PXP-7882",
  "RNX-5543",
  "RTC-0003",
  "RTC-0211",
  "SBD-0385",
  "SBX-0188",
  "SCH-0500",
  "SDY-0707",
  "SEP-0358",
  "SGY-0795",
  "SJE-0744",
  "SLS-0321",
  "SNC-0999",
  "SNH-0784",
  "SNT-0087",
  "SNT-0852",
  "SNZ-0807",
  "SOR-0323",
  "SPA-0056",
  "SRN-0348",
  "SRN-0507",
  "STE-0782",
  "STJ-0394",
  "STT-0787",
  "STV-0983",
  "STZ-0719",
  "SUC-0993",
  "SUL-0247",
  "SUR-0232",
  "SVL-0880",
  "TAM-0007",
  "TAX-0522",
  "TDD-0220",
  "TDD-0832",
  "TDD-0835",
  "TDD-0857",
  "TDG-0457",
  "TDT-0245",
  "TDX-0223",
  "TEC-0899",
  "TED-0040",
  "TEI-0482",
  "TEM-0771",
  "TFB-0008",
  "TFC-0547",
  "TGN-0458",
  "TGN-0689",
  "TGN-0887",
  "TGR-0846",
  "THB-6444",
  "TIO-0848",
  "TLX-0462",
  "TMN-0826",
  "TOA-0797",
  "TOM-0910",
  "TON-0876",
  "TOR-0290",
  "TPM-0303",
  "TQW-5119",
  "TSH-0330",
  "TTH-0915",
  "TTQ-0461",
  "TUC-0560",
  "TUG-5431",
  "TUH-0751",
  "TUT-0485",
  "TXZ-0269",
  "TYL-0214",
  "TZC-0731",
  "UCN-0858",
  "UCN-0869",
  "UHC-5231",
  "ULD-0556",
  "ULM-0822",
  "URC-0595",
  "USY-0861",
  "UTA-0650",
  "UTC-0033",
  "UTC-0333",
  "UTC-0926",
  "UTT-0855",
  "UYT-0950",
  "VAC-0817",
  "VIN-0446",
  "VNS-0452",
  "VOX-7337",
  "VRT-0811",
  "VSJ-0889",
  "VTD-0010",
  "VWF-0030",
  "WCA-5102",
  "WCA-5158",
  "WCU-0052",
  "WCU-0569",
  "WDC-0953",
  "WES-0085",
  "WES-0090",
  "WIN-0946",
  "WSD-0978",
  "WSN-0071",
  "WTL-0055",
  "WTL-0464",
  "WTL-0555",
  "WUT-0375",
  "WUT-0988",
  "WXL-0781",
  "XTL-0924",
  "XZP-0756"
]

const Header = cardHeader(true, false);
class NewCPRReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lcrReportList: [], reportName: "", isReportName: false, name: '', isName: '', lcrList: [], isLcr: false, selectedLcr: '',
      lata2list: [], selectLata: '', isLata: false,
      data: [], page: 0, sort: [], filter: [], defaultRate: 0, isDefaultRate: false,
      pageSize: 0, total_page: 0,activeTab: '1',
      lad: {
        data: [], page: 0, sort: [], filter: [],
        pageSize: 0, total_page: 0,
      },
      downloadModalVisible: false,
      selReportId: -1,
      noLata: false,
      lataCarrier: CARRIER_LIST[0],
      modal: {
        isOpen: false,
        data: [], page: 0, sort: [], filter: [],
        pageSize: 0, total_page: 0, id: null, states: [],
        latas: [], npanxxs:[], carriers: [], npalist: [],
        state:'', lata: '', npanxx: '', carrier: '',
      },
    }
  }

  componentDidMount() {
    this.subscribeNotification();
    this.props.callApi(RestApi.getLCRReportList, response => {
      if (response.ok) {
        this.setState({lcrReportList: response.data});
      }
    });
    this.props.callApi(RestApi.getLataList2, res => {
      if (res.ok) {
        this.setState({lata2list: res.data})
      }
    })
  };

  componentWillUnmount() {
    if (this.eventSource) {
      this.eventSource.close();
      this.eventSource = undefined;
    }
  }
  subscribeNotification = () => {
    if (this.eventSource) {
      this.eventSource.close();
      this.eventSource = undefined;
    }
    if (!this.props.auth.token) return;
    const eventSource = new EventSource(process.env.REACT_APP_API_ENDPOINT + 'notification/subscribe?access_token=' + this.props.auth.token, {withCredentials: false});
    eventSource.onmessage = (event) => {
      try {
        const data = JSON.parse(event.data);
        if (data.message){
          this.fetchLAD();
        }
      } catch(ex){}
    };
    this.eventSource = eventSource
  };

  cprgenReport = () => {
    if (this.state.reportName === "") {this.setState({isReportName: true});return false;}
    if (this.state.selectedLcr === "") {this.setState({isLcr: true}); return false;}
    if (this.state.defaultRate === 0) {this.setState({isDefaultRate: true}); return false;}
    this.props.callApi(RestApi.createNewCPRReport, res => {
    }, {lcrReportId: this.state.selectedLcr, name: this.state.reportName, defaultRate: parseFloat(this.state.defaultRate)});
  };

  viewLAD = (id, summary) => {
    summary = summary.split(";");
    this.props.callApi(RestApi.searchNewCPRReportData1, res => {
      if (res.ok) {
        let { carriers, latas, npanxxs } = res.data;
        let npa = npanxxs.slice(0);
        latas.unshift("");
        npanxxs.unshift("");
        let npalist = [];
        console.log(npa);
        while (npa.length) npalist.push(npa.splice(0, 10));
        this.setState({modal: produce(this.state.modal, m=> {
            m.isOpen = true; m.id = id; m.states = summary; m.carriers = carriers;
            m.latas = latas; m.state = summary[0]; m.npanxxs = npanxxs;
            m.npalist = npalist;
          })
        });
      }
    }, id, {
      state: summary[0].substring(0, 2)
    });

  };

  deleteItem = (id) => {
    this.props.callApi(RestApi.deleteNewCPRReport, res => {
      if (res.ok) {
        this.fetchLAD();
      }
    }, id)
  };

  fetchData = () => {
    this.fetchTimer && clearTimeout(this.fetchTimer);
    this.fetchTimer = setTimeout(()=>this._fetchData(), 500)
  };
  _fetchData = () => {
    let sorts = [];
    let filters = [];
    if (this.state.modal.sort.length) {
      let sorted = this.state.modal.sort;
      let column = sorted[0][0].id;
      let direction = sorted[0][0].desc;
      if (direction === false) direction = "asc";
      else if (direction === true) direction = "desc";
      let sort = {
        "column": column,
        "direction": direction
      };
      sorts.push(sort)
    }
    if (this.state.modal.filter.length) {
      let filtered = this.state.modal.filter;
      filtered = filtered[0];
      let filter = {};
      for (let i = 0; i < filtered.length; i++) {
        filter = {
          "column": filtered[i].id,
          "contains": filtered[i].value
        };
        filters.push(filter);
      }
    }
    let data = {
      page: this.state.modal.page,
      pageSize: this.state.modal.pageSize === 0 ? 10 : this.state.modal.pageSize,
      sorts: sorts,
      filters: filters
    };
    this.props.callApi(RestApi.searchNewCPRReportData1, response => {
      if (response.ok) {
        console.log(response.data);
        this.setState({modal: produce(this.state.modal, m=> {
            m.data= response.data.rows;
            m.total_page = response.data.totalPages;
          })})
      }
    }, this.state.modal.id, data);
  };
  fetchLAD = () => {
    this.fetchTime && clearTimeout(this.fetchTime);
    this.fetchTime = setTimeout(this._fetchLAD, 500)
  };

  _fetchLAD = () => {
    let sorts = [];
    let filters = [];
    if (this.state.lad.sort.length) {
      let sorted = this.state.lad.sort;
      let column = sorted[0][0].id;
      let direction = sorted[0][0].desc;
      if (direction === false) direction = "asc";
      else if (direction === true) direction = "desc";
      let sort = {
        "column": column,
        "direction": direction
      };
      sorts.push(sort)
    }
    if (this.state.lad.filter.length) {
      let filtered = this.state.lad.filter;
      filtered = filtered[0];
      let filter = {};
      for (let i = 0; i < filtered.length; i++) {
        filter = {
          "column": filtered[i].id,
          "contains": filtered[i].value
        };
        filters.push(filter);
      }
    }
    let data = {
      page: this.state.lad.page,
      pageSize: this.state.lad.pageSize === 0 ? 10 : this.state.lad.pageSize,
      sorts: sorts,
      filters: filters
    };
    this.props.callApi(RestApi.searchNewCPRReport, response => {
      if (response.ok) {
        console.log(response.data);
        this.setState({lad: produce(this.state.lad, m=> {
            m.data= response.data.rows;
            m.total_page = response.data.totalPages;
          })})
      }
    }, data);
  };

  viewLCRData = () => {
    this.fetchTimer && clearTimeout(this.fetchTimer);
    this.fetchTimer = setTimeout(this._viewLCRData, 500)
  };

  _viewLCRData = () => {
    let sorts = [];
    let filters = [];
    if (this.state.modal.sort.length) {
      let sorted = this.state.modal.sort;
      let column = sorted[0][0].id;
      let direction = sorted[0][0].desc;
      if (direction === false) direction = "asc";
      else if (direction === true) direction = "desc";
      let sort = {
        "column": column,
        "direction": direction
      };
      sorts.push(sort)
    }
    if (this.state.modal.filter.length) {
      let filtered = this.state.modal.filter;
      filtered = filtered[0];
      let filter = {};
      for (let i = 0; i < filtered.length; i++) {
        if (filtered[i].id === "role") {
          if (filtered[i].value === "all") {
            filter = {};
          } else {
            filter = {
              "column": "role_id",
              "exact": filtered[i].value
            }
          }
        } else {
          filter = {
            "column": filtered[i].id,
            "contains": filtered[i].value
          };
        }
        filters.push(filter);
      }
    }
    let data = {
      page: this.state.modal.page,
      pageSize: this.state.modal.pageSize === 0 ? 10 : this.state.modal.pageSize,
      sorts: sorts,
      filters: filters
    };
    this.props.callApi(RestApi.getCprReportDataById, response => {
      if (response.ok) {
        this.setState({
          modal: produce(this.state.modal, m => {
            m.data = response.data.rows;
            m.total_page = response.data.totalPages
          })
        })
      }
    }, this.state.modal.id, data);
  };

  handleRefresh = () => {this.fetchLAD()};

  handleUpdate = (type, value) => {
    console.log(type, value);
    this.setState({
      modal: produce(this.state.modal, m => {
        m[type] = value
      })
    })
  };


  toggleModal = () => {
    const modal = produce(this.state.modal, m => {
      m.isOpen = !m.isOpen;
    });
    this.setState({modal});
  };

  toggleLADModal = () => {
    const modal = produce(this.state.ladModal, m => {
      m.isOpen = !m.isOpen;
    });
    this.setState({ladModal: modal});
  };
  toggle = (tab) => {this.state.activeTab !== tab && this.setState({activeTab: tab});};

  ladGeneration = () => {
    if (this.state.selectLata === "") {
      this.setState({isLATA: true});
      return false;
    }
    if (this.state.name === "") {
      this.setState({isName: true});
      return false;
    }
    this.props.callApi(RestApi.createNewCPRReport, response => {},
      {
        secondReportId: this.state.selectLata,
        name: this.state.name
      })
  };

  onDownload = (id) => {
    this.setState({ selReportId: id, downloadModalVisible: true })
  }

  hideDownloadModal = () => {
    this.setState({ downloadModalVisible: false })
  }

  downloadReport = () => {
    this.downloadForm.action = process.env.REACT_APP_API_ENDPOINT + "cprgen/new_cpr_report/view1/" + this.state.selReportId + '/fulldownload/' + this.state.noLata + '/' + this.state.lataCarrier
    this.textInput.value = this.props.auth.token;
    this.downloadForm.submit();
    this.textInput.value = "";

    this.setState({ downloadModalVisible: false })
  }

  ladColumns = [
    {
      Header: 'Name',
      accessor: 'name',
      filterable: true,
      Cell: props => <div className="text-center">{props.value}</div>
    },
    {
      Header: 'Used LATA/NPANXX 2 Report Name',
      accessor: 'second_report_name',
      filterable: true,
      Cell: props => <div className="text-center">{props.value}</div>
    },
    {
      Header: 'Created At',
      accessor: 'created_at',
      Cell: props => <div className='text-center'>{formatDate(props.value)}</div>
    },
    {
      Header: 'Updated At',
      accessor: 'updated_at',
      Cell: props => <div className='text-center'>{formatDate(props.value)}</div>
    },
    {
      Header: 'Action',
      accessor: 'id',
      width: 400,
      Cell: props => <div className="text-center">
        <Button size="sm" color="primary" onClick={() => this.viewLAD(props.value, props.original.v1_summary)}>
          <i className="fa fa-eye"></i>
        </Button>

        <Button size="sm" color="success" className="ml-2" onClick={() => {this.onDownload(props.value)}}>
          <i className="fa fa-download"></i>
        </Button>

        <Button size="sm" color="danger" onClick={() => this.deleteItem(props.value)} className="ml-2">
          <i className="fa fa-close"></i>
        </Button>
      </div>
    },
  ];


  render() {
    return (
      <Row>
        <Col lg="12">
          <Card>
            <Header>CPR Report Generation</Header>
            <CardBody>
              <FormGroup row>
                <Col lg="4" className="row">
                  <Col lg="6" className="text-right">
                    <Label className="font-weight-bold">CPR Report Name:</Label>
                  </Col>
                  <Col lg="6">
                    <Input type="text" onChange={(ev)=> {this.setState({name: ev.target.value, isName: false});
                    }} className="form-control-sm" invalid={this.state.isReportName}/>
                    {this.state.isName ? <FormFeedback>Please input name!</FormFeedback> : ""}
                  </Col>
                </Col>
                <Col lg="6" className="row">
                  <Col lg="6" className="text-right">
                    <Label className="font-weight-bold">LATA/NPANXX 2 List:</Label>
                  </Col>
                  <Col lg="6">
                    <Input type="select" onChange={(ev)=> {this.setState({selectLata: ev.target.value, isLata: false});
                    }} className="form-control-sm" invalid={this.state.isLata} required>
                      <option value=""/>
                      {this.state.lata2list.map(({id, name}) => {
                        return <option value={id}>{name}</option>
                      })}
                    </Input>
                    {this.state.isLata ? <FormFeedback>Please select LATA/NPANXX 2 report!</FormFeedback> : ""}
                  </Col>
                </Col>
              </FormGroup>
            </CardBody>
            <CardFooter>
              <Button size="md" color="primary" onClick={this.ladGeneration}>Generation CPR Report</Button>
            </CardFooter>
          </Card>
          <Card>
            <CardHeader>
              <Row>
                <Col><strong className="card-title-big">CPR Report List</strong></Col>
                <Col>
                  <div className="text-right">
                    <Button size="md" color="link" onClick={this.handleRefresh}><i className="fa fa-refresh"/> Refresh</Button>
                  </div>
                </Col>
              </Row>
            </CardHeader>
            <CardBody>
              <ReactTable
                manual
                data={this.state.lad.data}
                columns={this.ladColumns}
                defaultPageSize={10}
                onFilteredChange={(filter) => {
                  let filters = [];
                  filters.push(filter);
                  this.setState({lad: produce(this.state.lad, m=> {
                      m.filter = filters;
                    })});
                }}
                onSortedChange={(sort) => {
                  let sorts = [];
                  sorts.push(sort);
                  this.setState({lad: produce(this.state.lad, m=> {
                      m.sort = sorts;
                    })});
                }}
                onPageChange={
                  (page)=>this.setState({lad: produce(this.state.lad, m=> {
                      m.page = page;
                    })})
                }
                onPageSizeChange={(pageSize) => this.setState({lad: produce(this.state.lad, m=> {
                    m.pageSize = pageSize;
                  })})}
                minRows={this.state.lad.data.length && this.state.lad.data.length}
                pages={this.state.lad.total_page}
                onFetchData={this.fetchLAD}
              />
            </CardBody>
          </Card>
        </Col>
        <ViewLADModal
          isOpen={this.state.modal.isOpen}
          toggle={this.toggleModal}
          id={this.state.modal.id}
          handler={this.handleUpdate}
          data={this.state.modal}
        />

        {this.renderDownloadModal()}

        <form ref={(node)=> {this.downloadForm = node}} action="" target="_blank" method="post">
          <input type="hidden" ref={(input)=> {this.textInput = input}} name="access_token" value=""/>
        </form>
      </Row>
    );
  }


  renderDownloadModal = () => (
    <Modal isOpen={this.state.downloadModalVisible} toggle={this.hideDownloadModal} className={'modal-md ' + this.props.className}>
      <ModalHeader toggle={this.hideDownloadModal}>Confirm For LATA</ModalHeader>
      <ModalBody>
        <Label>Please select "lata" or "no lata":</Label>
        <div className="row" style={{display: "flex", justifyContent: 'flex-end'}}>
          <div className="form-check align-content-center col-6 ml-1">
            <Input type="radio" className="form-control-sm" style={{'marginTop': '-0.3rem'}} id="lata" name="lata"
                   checked={!this.state.noLata} onChange={() => { this.setState({noLata: false})}} />
            <label className="form-check-label" htmlFor="lata">Lata</label>
            <label style={{marginLeft: '20px'}} htmlFor="lataCarrier">Default Carrier: </label>

            <Input type="select" className="form-control-sm" style={{marginLeft: '50px', width: '150px'}} id="lataCarrier" name="lataCarrier" onChange={(ev) => {this.setState({lataCarrier: ev.target.value})}} value={this.data!=undefined && this.data!= null ? this.data.lataCarrier : ''}>
              {CARRIER_LIST.map(s => <option value={s}>{s}</option>)}
            </Input>
          </div>
          <div className="form-check col-5">
            <Input type="radio" className="form-control-sm" style={{'marginTop': '-0.3rem'}} id="noLata" name="noLata"
                   checked={this.state.noLata} onChange={() => { this.setState({noLata: true})}} />
            <label className="form-check-label" htmlFor="noLata">No Lata</label>
          </div>
        </div>
      </ModalBody>
      <ModalFooter>
        <Button type="submit" size="md" color="primary" className="mr-2" onClick={this.downloadReport}> Download</Button>
        <Button type="reset" size="md" color="danger" onClick={this.hideDownloadModal}> Cancel</Button>
      </ModalFooter>
    </Modal>
  );

  renderNavbar = (id, name) => {
    return  <NavItem>
      <NavLink className={classnames({active: this.state.activeTab === id})} onClick={() => {this.toggle(id);}}>
        <Label className="font-weight-bold"><span style={{fontSize: 22}}> {name}</span></Label>
      </NavLink>
    </NavItem>
  };
}

export default withAuthApiLoadingNotification(NewCPRReport);
